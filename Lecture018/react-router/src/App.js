import { Routes, Route } from 'react-router-dom';
import Home from './pages/Home';
import Contact from './pages/Contact';
import SearchJob from './pages/SearchJob';
import NoMatch from './pages/NoMatch';
import JobDetails from './pages/JobDetails'; 
import "./App.css"

const App = () => {
  return (
    <>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/contact" element={<Contact />} />
        <Route path="/search-job" element={<SearchJob />} />
        <Route path="/detail/:id" element={<JobDetails/>} />
        <Route path="*" element={<NoMatch />} />
      </Routes>
    </>
  );
};

export default App;
