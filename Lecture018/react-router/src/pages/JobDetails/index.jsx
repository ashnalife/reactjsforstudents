import { useEffect, useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
const JobDetails = () => {
    const { id } = useParams();
    const nevigate = useNavigate();
    const [jobDetails, setJobDetail] = useState({
      by: "XXXX",
      id: 0,
      score: 0,
      time: 0,
      title: "XXXX",
      type: "XXXX",
      url: "XXXX",
    });
    useEffect(()=>{
        fetch(`https://hacker-news.firebaseio.com/v0/item/${id}.json`)
        .then(res=>res.json())
        .then((jsonRes)=>{
            setJobDetail(jsonRes);
        }).catch(e=>{
            console.log(e);
            nevigate("fallback");//offline case switch of network to test fallback
        });
    },[id]);
    return (<div>
        <h2>{jobDetails.id}</h2>
        <h2>{jobDetails.title}</h2>
        <p>{jobDetails.by}</p>
        <p>{jobDetails.type}</p>
        <a href={jobDetails.url}>{jobDetails.url}</a>
    </div>);
}
export default JobDetails;