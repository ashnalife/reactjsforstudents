
import { useNavigate } from 'react-router-dom';
const Home =() => {
    const nevigate = useNavigate();
    return (
        <>
        <h1>Job Finder App</h1>
        <p>We help students to search for active jobs.</p>
        <p>Click on search button to search for active jobs.</p>
        <button onClick={()=>{nevigate('search-job')}}>Search Job</button>
        </>
    );
}
export default Home;