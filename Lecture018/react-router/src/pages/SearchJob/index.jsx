import { useEffect, useState } from "react";
import { useNavigate } from 'react-router-dom';

const SearchJob = () => {
    const nevigate = useNavigate();
    const [allJobs,setAllJobs] = useState([]);
    const [isLoading,setLoading]= useState(true);
    useEffect(()=>{
        fetch('https://hacker-news.firebaseio.com/v0/jobstories.json')
        .then(res=>res.json())
        .then((jsonRes)=>{
            setAllJobs(jsonRes);
            setLoading(false);
        }).catch(e=>{
            console.log(e);
            nevigate("fallback");//offline case switch of network to test fallback
        });
    },[]);

    const onClickOfJob = (job) => {
        nevigate(`/detail/${job}`);
    };
    return(
        <div className="push-right">
        <h1>SearchJob</h1>
        {isLoading ? <h3>Loading...</h3> :
        allJobs.map((job,index)=><p onClick={()=>{onClickOfJob(job)}} key={index} className="job-id">{job}</p>)
        }
        </div>
    )
};
export default SearchJob;