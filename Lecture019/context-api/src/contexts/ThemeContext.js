import {createContext, useState} from 'react';

const ThemeContext = createContext();


export const ThemeProvider =({children})=>{
    const[isDark, setIsDark] = useState(false);
    const toggleTheme = () => {
        setIsDark((prevTheme)=>!prevTheme);
    };

    return(
    <ThemeContext.Provider value={{ isDark, toggleTheme }}>
        {children}
    </ThemeContext.Provider>
    );
}

export default ThemeContext;


// The createContext() function in React returns an object that consists of two components:

// Provider: This component is used to wrap the parent hierarchy of components where you want to make the context available. 
    //It accepts a value prop that provides the data you want to share.

// Consumer: This component allows functional components to subscribe to the context and access the shared data using a render prop function.


