import React from "react";
import ThemeContext from "../contexts/ThemeContext";
import ContactPage from "./ContactPage";

const LandingPage = () => {
  return (
    <ThemeContext.Consumer>
      {({ isDark, toggleTheme }) => (
        <div>
          <button onClick={toggleTheme}>
            {isDark ? "Switch to Light Mode" : "Switch to Dark Mode"}
          </button>
          <ContactPage/>
        </div>
      )}
    </ThemeContext.Consumer>
  );
};

export default LandingPage;
