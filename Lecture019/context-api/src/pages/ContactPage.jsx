import { useContext } from "react";
import ThemeContext from "../contexts/ThemeContext";

export default ()=>{
    const {isDarkMode, toggleTheme} = useContext(ThemeContext);
    return isDarkMode ? <h1>Dark</h1>:<h>Light</h>;
}