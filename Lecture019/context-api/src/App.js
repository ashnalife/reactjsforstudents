
import { ThemeProvider } from "./contexts/ThemeContext";
import LandingPage from "./pages/LandingPage";
function App() {
  return (
    <div>
      <ThemeProvider>
      <LandingPage/>
      <h1>Test</h1>
    </ThemeProvider>
    </div>
  );
}

export default App;
