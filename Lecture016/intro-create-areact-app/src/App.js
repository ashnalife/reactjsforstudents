import { useCallback, useState } from 'react';
import useInput from './useInput';
import { useToggle } from './useToggle';


const Dummy = ()=>{
  return <h1>I am dummy</h1>
}
export const App = ({myName,myCity})=>{
  const [count,setCount] = useState(100);
  const [name,setName] = useState(myName);
  const onclickHandler = useCallback(()=>{
      setCount(count+1);
      setName(name+myCity);
    }
  );
  //onchangeHandler1
  const [val1, onchangeHandler1] = useInput();
  //onchangeHandler2
  const [val2, onchangeHandler2] = useInput();
  //onchangeHandler3
  const [val3, onchangeHandler3] = useInput();
  console.log("lalla",val1,val2,val3);

  const[toggleVal,onToggleHandler] = useToggle();
  return (
    <>
      <h1>{count}</h1>
      <h1>{name}</h1>
      <button onClick={onclickHandler}>Inc</button>
      <Dummy/>
      {/* Custom hooks */}
      <input placeholder='001' onChange={onchangeHandler1} value={val1}/>
      <input placeholder='002'onChange={onchangeHandler2} value={val2}/>
      <input placeholder='003' onChange={onchangeHandler3} value={val3}/>
      <button onClick={onToggleHandler}>Toggle Display</button>
      {toggleVal ? <h1>Show!</h1> : <h1>Hide!</h1>}
    </>
  );
}

export default Dummy;
