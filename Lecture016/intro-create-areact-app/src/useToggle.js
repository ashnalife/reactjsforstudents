import { useState } from "react"

export const useToggle =()=>{
    const [val,setVal] = useState(false);
    const onToggleHandler = () => {
        setVal(prevVal=> !prevVal);
    }
    return [val,onToggleHandler];
}