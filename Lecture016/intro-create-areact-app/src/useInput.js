import { useState } from "react"
const useInput = (initVal) => {
    const [val,setVal] = useState(initVal || "")
    const handler = (event) =>{
        setVal(event.target.value);
    }

    return [val,handler]
}
export default useInput;