import { get } from "http";
import { createContext, useContext, FC ,ReactNode} from "react";
type Task = {
    id:string,
    text:string,
};
type List = {
    id:string,
    text:string,
    tasks:Task[]
};
 type AppState = {
    lists: List[],
}
const appData: AppState = {
  lists: [
    {
      id: "0",
      text: "To Do",
      tasks: [{ id: "c0", text: "Generate app scaffold" }],
    },
    {
      id: "0",
      text: "In Progress",
      tasks: [{ id: "c0", text: "Generate app dcaffold" }],
    },
    {
      id: "0",
      text: "Done",
      tasks: [{ id: "c0", text: "Generate app ecaffold" }],
    },
  ],
};

type AppStateContextProps = {
    lists:List[],
    getTasksByListId(id:string):Task[]
};

const AppStateContext = createContext<AppStateContextProps>({} as AppStateContextProps);
type AppStateProvidorProps = {
    children: ReactNode,
};
export const AppStateProvidor: FC<AppStateProvidorProps> = ({ children }:AppStateProvidorProps) => {
  const { lists } = appData;
  const getTasksByListId = (id: string) => {
    return lists.find((list) => list.id === id)?.tasks || [];
  };
  return (
    <AppStateContext.Provider value={{ lists, getTasksByListId }}>
      {children}
    </AppStateContext.Provider>
  );
};


export const useAppStateContext = () => {
    return useContext(AppStateContext);
}