

interface AddListAction {
    type:'ADD-LIST',
    payload:string

};

interface AddTaskAction {
    type:'ADD-TASK',
    payload:{ text: string; listId: string }
}

export type Action = AddListAction | AddTaskAction;

export const addTaskAction = (text:string,listId:string):Action =>{
    return {
        type:'ADD-TASK',
        payload:{
            text,
            listId
        }
    }
}

export const addListAction =(text:string):Action=>{
    return {
        type:'ADD-LIST',
        payload:text
    }
}