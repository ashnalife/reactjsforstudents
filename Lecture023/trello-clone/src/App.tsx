import * as React from "react";

import "./App.css";
import { Column } from "./components/Column";
import { AppContainer } from "./styles";
import { AddNewItem } from "./components/AddNewItem";
import { useAppStateContext } from "./contexts/AppStateContext";

interface Props {
  children: React.ReactNode;
}

export const App: React.FC<Props> = ({ children }) => {
  const { lists } = useAppStateContext();
  return (
    <AppContainer>
      {lists.map((list) => (
        <Column text={list.text} key={list.id} id={list.id} />
      ))}
      <AddNewItem toggleButtonText="+ Add another list" onAdd={console.log} />
    </AppContainer>
  );
};

export default App;
