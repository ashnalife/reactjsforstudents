import { useState,KeyboardEvent } from "react";
import { NewItemButton, NewItemFormContainer,NewItemInput } from "../styles";
import { useFocus } from "../hooks/useFocus";

type NewItemFormProps = {
    onAdd(text:string):void,
};
export const NewItemForm = ({onAdd}:NewItemFormProps) => {
    const [text,setText] = useState<string>("");
    const inputRef = useFocus();
    const handleAddText = (event:KeyboardEvent) => {
        console.log(event);
        if(event.key==='Enter'){
            onAdd(text);
        }
    }

    return(
        <NewItemFormContainer>
            <NewItemInput
            ref={inputRef}
            value={text}
            onChange={e=>setText(e.target.value)}
            onKeyDown={handleAddText}
            />
            <NewItemButton onClick={()=>onAdd(text)}>
                Create
            </NewItemButton>
        </NewItemFormContainer>
    );
};