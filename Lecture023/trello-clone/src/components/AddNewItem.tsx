import { useState } from "react";
import { AddItemButton } from "../styles";
import { NewItemForm } from "./NewItemForm";

type AddNewItemProp = {
    onAdd(text:string):void,//is a callback
    toggleButtonText:string,
    dark?:boolean,
};

export const AddNewItem = (props:AddNewItemProp) => {
    const {dark,onAdd,toggleButtonText} = props;
    const [showForm, setShowForm] = useState<boolean>(false);
    if(showForm){
        return(
            <NewItemForm
            onAdd = {text=>{
                onAdd(text);
                setShowForm(false);
            }}
            />
        );
    }
    return (
        <AddItemButton dark={dark} onClick={()=>setShowForm(true)}>
            {toggleButtonText}
        </AddItemButton>
    );
}