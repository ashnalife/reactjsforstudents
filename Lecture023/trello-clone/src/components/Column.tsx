import  * as React from 'react';
import { ColumnContainer, ColumnTitle } from "../styles";
import { AddNewItem } from './AddNewItem';
import { useAppStateContext } from '../contexts/AppStateContext';
import { Card } from './Card';

type ColumnProps = {
    text?: string | undefined,
    id:string,
};

export const Column:React.FC<ColumnProps> = ({text,id}) => {
  const { getTasksByListId } = useAppStateContext()
  const tasks = getTasksByListId(id)
  return (
    <ColumnContainer>
      <ColumnTitle>{text ? text : "Default Title"}</ColumnTitle>
      {tasks.map((task) => (
        <Card text={task.text} key={task.id} id={task.id} />
      ))}
      <AddNewItem
        toggleButtonText="+ Add another task"
        onAdd={console.log}
        dark
      />
    </ColumnContainer>
  );
};
