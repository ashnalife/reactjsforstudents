import {FC} from 'react';
import {CardContainer} from "../styles";

type CardProp = {
    text:string,
    id:string,
};

export const Card:FC<CardProp> =({text,id}:CardProp)=>{
    return <CardContainer>{text}</CardContainer>
}