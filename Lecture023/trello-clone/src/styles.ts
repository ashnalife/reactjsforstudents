import styled from 'styled-components';

// Advantages of CSS-in-JS:

// Scoped Styles: CSS-in-JS automatically scopes styles to components, reducing the chances of style conflicts and making it easier to reason about your application's styling.

// Dynamic Styles: CSS-in-JS allows you to create dynamic styles based on component props or other conditions, making it versatile for responsive design and theming.

// Ease of Maintenance: Styles are co-located with components, making it easier to find and update them. This can improve code maintainability.

// Dead Code Elimination: Some CSS-in-JS libraries can perform dead code elimination, removing unused styles from the final bundle, potentially reducing file size.

// Optimizations: CSS-in-JS libraries often provide optimizations like automatic vendor prefixing and server-side rendering support.

// Developer Experience: Developers who are comfortable with JavaScript may find CSS-in-JS more natural and prefer working in a single language.


// Performance Overhead: CSS-in-JS can introduce a runtime overhead, especially for complex applications. However, this overhead is usually negligible for most projects.

export const AppContainer = styled.div`
background-color:#3179ba;
display: flex;
flex-direction: row;
align-items: flex-start;
height: 100%;
padding: 1.5em;
width: 100%;
`;
export const ColumnContainer = styled.div`
background-color:#ebecf0;
width: 30vw;
min-height: 2em;
border-radius: 3px;
padding: 0.5em 0.5em;
flex-grow: 0;
margin-right: 0.5em;
`;
export const CardContainer = styled.div`
background-color: #fff;
cursor: pointer;
margin-bottom: 0.5em;
padding: 0.5em 1em;
max-width: 100%;
border-radius: 3px;
box-shadow: #091e4240 0px 1px 0px 0px;
`;

export const ColumnTitle = styled.div`
padding: 0.5em;
font-weight: bold;
`;

type AddItemButtonProps = {
    dark?: boolean,
};

export const AddItemButton = styled.button<AddItemButtonProps>`
background-color: #ffffff3d;
border-radius: 3px;
border: none;
color: ${(props)=>props.dark?'#000':'#fff'};
cursor: pointer;
max-width: 30vw;
width: 100%;
padding: 1em;
text-align: left;
transition: background 85ms ease-in;
&:hover{
    background-color: #ffffff52;
}
`;

export const NewItemFormContainer = styled.div`
max-width: 30vw;
display: flex;
flex-direction: column;
width: 100%;
align-items: flex-start;
`;

export const NewItemButton = styled.button`
    background-color: #5aac44;
    border-radius: 3px;
    border: none;
    box-shadow: none;
    columns: #fff;
    padding: 1em;
    text-align: center;
`;

export const NewItemInput = styled.input`
border-radius: 3px;
border: none;
box-shadow: #091e4240 0px 1px 0px 0px;
margin-bottom: 0.5em;
padding: 0.5em 1em;
width: 100%;
`;


