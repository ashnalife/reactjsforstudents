import logo from './logo.svg';
import { AuthHOC } from './hoc/AuthHoc';
import './App.css';

function App() {
  return AuthHOC(<h1>This is user token {sessionStorage.getItem('token')}</h1>);
}

export default App;
