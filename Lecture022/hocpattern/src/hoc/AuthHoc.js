
import {useState,useEffect} from 'react';

export const AuthHOC = (component) => {
    const [authenticated,setAuthenticated] = useState(false);
    useEffect(()=>{
        let temp = sessionStorage.getItem("token");//session and local storage is synchronous
        // do api call here . for validation.
        if(temp === 'secret-token'){
            setAuthenticated(true);
        }
        //or nevigate to login page here.
        //nevigate('login')
    },[]);

    return authenticated ? component : <h3>Log in to see this.</h3>; 
};