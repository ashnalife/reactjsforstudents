
import { useCallback, useState } from "react";
import TimerForm from "./components/CreateTimerForm";
import TimerContainer from "./containers/TimerContainer";
import AddWidget from "./components/AddWidget";

function App() {
  const [openForm, setOpenForm] = useState(false);
  const [timersList,setTimersList] = useState([]);
  const [isEditMode,setIsEditMode] = useState(false);
  const [editedIndex,setEditedIndex] = useState(-1);

  const addTimer = useCallback((timerName,timerDuration) => {
    const timerObj = {
      name:timerName,
      duration:timerDuration,
    };
    setTimersList((prevTimerList)=>[...prevTimerList,timerObj]);
  });
  const onDeleteOfTimer = (index) => {
    setTimersList((prevTimersList)=>prevTimersList.filter((item,i)=>i!==index));
  };
  const onEditOfTimer = (index) => {
    console.log("onEditOfTimer",index);
    setIsEditMode(true); 
    setOpenForm(true);
    setEditedIndex(index);
  }
  const onUpdateHandler = (name,duration,index) => {
      const timerObj = {
        name:name,
        duration:duration
      };
      const timerListCopy = [...timersList];
      timerListCopy[index]=timerObj;
      setTimersList(timerListCopy);
  }

  return (
   <div className="root">
    <AddWidget 
      onAddWidget={()=>{setOpenForm(true)}}
      />
   

      {isEditMode? <TimerForm 
      showForm={openForm} 
      onCloseForm={()=>{setOpenForm(false);setIsEditMode(false);setEditedIndex(-1)}}
      onUpdateHandler = {onUpdateHandler}
      editData={timersList[editedIndex]}
      editedIndex={editedIndex}
      />: <TimerForm 
      showForm={openForm} 
      onCloseForm={()=>{setOpenForm(false)}}
      onCreateHandler = {addTimer}
      editData={null}
      />}
      
      {timersList.map((timer,index)=>{
        return (<div className="timer-list-container" key={index}>
            <TimerContainer
            timerName={timer.name} 
            timerDuration={timer.duration}
            onDeleteOfTimer={onDeleteOfTimer}
            onEditOfTimer={onEditOfTimer}
            timerIndex={index}
            />
        </div>);
      })}
    
   </div>
  );
}

export default App;
