
import TimerDisplay from "../../components/TimerDisplay"
import "./timerContainer.css";
import { MdEdit,MdDeleteForever } from "react-icons/md";
const TimerContainer = (
    {   timerName,
        timerDuration,
        onDeleteOfTimer,
        timerIndex, 
        onEditOfTimer   
    }) => {
    return (
        <div className="container">
            <h2>{timerName}</h2>
            <TimerDisplay timerVal={timerDuration}/>
            <div className="icon-container">
                <div className="icon-style" onClick={()=>{onEditOfTimer(timerIndex)}}><MdEdit/></div>
                <div className="icon-style" onClick={()=>{onDeleteOfTimer(timerIndex)}}>
                    <MdDeleteForever/>
                </div>
            </div>
        </div>
    );
};

export default TimerContainer;