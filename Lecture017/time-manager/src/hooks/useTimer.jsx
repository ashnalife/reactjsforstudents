import { useEffect, useState, useRef } from "react";

export const useTimer = (startingTime) => {
    const [time, setTime] = useState(startingTime);
    let timerRef = useRef(null);
    const timerCreator = () => {
        return setInterval(() => {
            setTime((prevTime) => (prevTime === 0 ? prevTime : prevTime - 1));
        }, 1000);
    }
    useEffect(() => {
        timerRef = timerCreator();
        return () => {
            clearInterval(timerRef);
        };
    }, []);
    useEffect(() => {
        setTime(startingTime);//reinitialise state
        clearTimeout(timerRef);
        timerRef=timerCreator();
    }, [startingTime]);

    return time;
};