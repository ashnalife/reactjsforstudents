import { createPortal } from "react-dom";
import "./timerForm.css";
import { useState } from "react";
export default ({
  showForm,
  onCloseForm,
  onCreateHandler,
  onUpdateHandler,
  editData,
  editedIndex
}) => {
  const [name, setName] = useState(editData?.name ? editData.name : "");
  const [duration, setDuration] = useState(editData?.duration ? editData.duration : "");
  const [showError, setShowError] = useState(false);
  const [errorType, setErrorType] = useState("");
  //instead use custom hook make it dry.
  const nameHandler = (event) => {
    setName(event.target.value);
  };

  const durationHandler = (event) => {
    setDuration(event.target.value);
  };

  const validateAndSubmit = () => {
    if (name === "") {
      setShowError(true);
      setErrorType("Invalid Timer Name.");
      return;
    }
    if (duration === "" || !/^\d+$/.test(duration)) {
      setShowError(true);
      setErrorType("Invalid Duration Name.");
      return;
    }
    console.log("editData",editData);
    if(editData){
        onUpdateHandler(name,duration,editedIndex)
    }else{
        onCreateHandler(name, duration);
    }
    
    //state initialise
    setName("");
    setDuration("");
    onCloseForm();
  };

  return (
    showForm &&
    createPortal(
      <div className="form-wrapper">
        <input
          placeholder="Timer Name"
          className="input-style"
          onChange={nameHandler}
          value={name}
        />
        <input
          placeholder="Timer Duration"
          className="input-style"
          onChange={durationHandler}
          value={duration}
        />
        <button className="button-priary" onClick={validateAndSubmit}>
          {editData? "Edit Timer" : "Create Timer"}
        </button>

        <button className="button-priary" onClick={onCloseForm}>
          close
        </button>
        {showError ? (
          <h3 className="error-msg">{errorType}</h3>
        ) : (
          <h3 className="error-msg"></h3>
        )}
      </div>,
      document.body
    )
  );
};
