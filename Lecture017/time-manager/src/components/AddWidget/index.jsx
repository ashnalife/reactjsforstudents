import "./addWidget.css";

const AddWidget = ({onAddWidget}) => {
    return <div className="add-widget" onClick={onAddWidget}>
        +
    </div>
}

export default AddWidget;